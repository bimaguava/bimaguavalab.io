---
title: "Tips merubah warna objek"
date: 2019-08-26T16:30:10+07:00
tags: photoshop
categories: design
cover: img/ps1.jpg
---

# Intro
Dalam mengubah warna suatu gambar, tidak cukup dengan menggunakan cara instan, dengan menggeser ```hue saturation``` saja. Sehingga hasilnya mempengaruhi seluruh objek. Karena kita membutuhkan seleksi untuk objek yang diinginkan.

Ada beberapa teknik yang dapat mempermudah karena ecara umum kita hanya menyeleksi bagian yang dipilih, lalu memberi warna dengan ```solid color```, lalu memberi efek gelap terang dengan diagram ```curves```. 


Efek gelap terang
berfungsi untuk mendapatkan warna yang menyatu dengan lekukan pada objek


Warna solid
Sedangkan memberi warna solid artinya satu blok warna (tanpa efek transparan)


<hr/>

# Alur kerja
Secara keseluruhan yang dilakukan dari awal sampai akhir ada 3 tahap pengerjaan, yaitu

- **Solid color**
digunakan untuk memberikan warna solid pada objek
- **Curves**
digunakan untuk memberikan efek gelap-terang sehingga tercipta lekukan pada objek
- **Blend if**
digunakan untuk memberikan pantulan cahaya pada objek sehingga objek terlihat sempurna

Kelihatan cukup simpel, tidak serumit jika dengan https://www.gimp.org/about/, GIMP, title untuk melakukan ini hehe..

# Langkah-langkah

## Memberi warna solid ke kemeja

1. Sebelum mengutak-atik gambar, bikin backup dulu untuk jaga2 dengan **menduplicate layer**, ```[CTRL + j]``` 
	
2. Seleksi dulu area yang diinginkan (Kemeja), pakai **Quick Selection Tool** atau ```[W]```
	
3. Baru setelah itu, kita buat layer mask, Klik **Add layer mask** yang berada disamping kiri adjusment layer (kotak bulat)
	
4. Setelah sudah menambahkan layer mask, di step ini kita akan **mewarnai kemeja**. Klik ```Adjusment layer > Solid Color```. Pilih warnanya

![images001](https://res.cloudinary.com/bimagv/image/upload/v1592389732/2019-08/ps1_xcaohx.png "images001")

5. **Klik kanan** tepat di layer yang digunakan saat ini dan pilih **Create clipping mask**. Maka kita akan mendapatkan sebuah warna yang solid untuk kemeja

## Efek lekukan

Nah kita tadi sudah beri warna pada kemeja. Tapi, hasil warna masih sangat *solid*, biar terlihat lekukan bajunya **ganti efeknya jadi Multiply**

## Efek gelap & realistic

Selanjutnya adalah ntuk mengingkatkan efek gelap/shadow, **pakai tool Curves** dengan cara klik ```Adjusement layer > Curves```

Jangan lupa **klik kanan** pada layer dan **create Clipping mask** untuk efek gelap-terangnya.
Cara pakainya tinggal pilih icon tangan untuk mengatur kecerahan dengan tahan klik kiri turun/naikkan area gelapnya. Lakukan juga pada area terangnya. 

![images002](https://res.cloudinary.com/bimagv/image/upload/v1592389770/2019-08/ps2_ogf2nq.png "images002")

Cukup simpel ✨

Namun, sampai pada metode Curves tersebut masih belum menunjukkan pantulan cahaya dari kemeja

Solusinya ialah menggunakan **blend if**

Tekniknya adalah membuat warna kemeja terang dahulu, lalu kita akan menggelapkan mulai dari sudut objek sampai memenuhi dalam objek.

1. Pakai tools Curvers lagi ```Adjusment layer > Curves```, dan pilih **'create clipping mask'**. Naikkan kecerahannya sampai terang benderang

2. Lalu, buka layer style dengan cara **klik 2x** pada layer yang digunakan saat ini dan atur **'Underlying layer'** dengan menggeser disertai Alt untuk membagi 2 nodenya supaya tidak ada efek kasar pada kemeja

![images003](https://res.cloudinary.com/bimagv/image/upload/v1592389764/2019-08/ps3_sjqjzw.png "images003")


# Hasil akhir
Setelah semua selesai kita bisa bebas merubah warna kemeja dengan klik 2x layer solid color

![images004](https://res.cloudinary.com/bimagv/image/upload/v1592389785/2019-08/ps4_q514lr.png "images004")

# Kesimpulan

Ringkasan ini mudah-mudahan bisa berguna dan masih perlu update kedepannya untuk itu silahkan bereksplorasi sendiri untuk mencari resource terbaru berkaitan hal ini.

Walaupun begitu menurut saya pencahayaan yang dihasilkan sudah lumayan bagus saat ini, mungkin kerapihan dalam melakukan seleksi pada objek memegang peran penting disini karena dengan rapihnya area terseleksi akibatnya sudut luar objek akan memberi kesan lebih untuk hasil keseluruhan.

Mudah-mudahan bermanfaat untuk teman-teman.

Terima kasih.
---
title: "PhotoGIMP: patch UI untuk GIMP 2.10"
date: 2020-07-02 16:01:34
tags: gimp
categories: design
cover: https://res.cloudinary.com/bimagv/image/upload/v1593600228/Grey_Circles_Gaming_Youtube_Channel_Art_1_ovdjny.jpg
---


📖 Tutorial ini saya buat menggunakan Linux. So, teman-teman silahkan menyesuaikan (^_^)

Pengguna Windows lihat [disini](https://www.youtube.com/watch?v=57DNUsf4A-0) 
Mac [disini](https://www.youtube.com/watch?v=5nXhtaGQs9U)

<hr/>

# Preface
**PhotoGIMP** adalah sebuah patch yang dapat digunakan untuk mendekorasi tampilan pada GIMP sehingga "*mirip dengan photoshop*". Sehingga awal melihat info tersebut saya pun sangat tertarik :) 

Namun, tujuan project itu bukan untuk seolah menyaingi fitur dewa sotosop --*ya memang bukan main*-- melainkan mempermudah user GIMP yang memang dari dulu *tercemplung* ke dalam lingkungan photoshop.

![images001](https://memegenerator.net/img/instances/73342641/we-must-act-with-proprietary.jpg "images001")

mau gimana lagi ye khan.



Project ini dibuat oleh [Diolinux](https://www.youtube.com/user/Diolinux) yang memuat banyak fitur dan juga konfigurasi yang membuat anda terpesonah, gitu.

# Daftar patch
1. Tampilan apik dan juga splashcreen yang mantul
2. segudang font (https://github.com/Diolinux/PhotoGIMP/blob/master/fonts.txt)
3. filter **Heal Selection**
4. Keybinds atau shortcut yang pastinya pengguna photohsop sudah familiar (https://helpx.adobe.com/photoshop/using/default-keyboard-shortcuts.html)
5. udah 

# Cara memasang PhotoGIMP

## GIMP 2.10
Yang kita butuhkan tentu saja ```GIMP``` itu sendiri dengan versi **minimal 2.10**. Dan GIMP yang di butuhkan oleh ```PhotoGIMP``` ialah paket yang berasal dari flatpak. jika menggunakan *self-contained program* lain, snap dan semacamnya sepertinya tidak bisa.

Dengan menggunakan flatpak anda akan mengunduh banyak sekali dependensi dibandingkan menggunakan software manager, seperti ```apt``` atau sejenisnya, jadi pastikan wifi tetangga aman.

Tahap instalasinya sebagai berikut
```
sudo apt install flatpak
flatpak install flathub org.gimp.GIMP
```

## PhotoGIMP
Sekarang kita menuju halaman https://github.com/Diolinux/PhotoGIMP

![images001](https://res.cloudinary.com/bimagv/image/upload/v1593685311/2020-07/images001_z1o9eb.png "images001")

Lalu, Download ZIP. 

Nah, kita hanya akan memindahkan file patch yang dibutuhkan untuk mendapatkan hasil untuk ```GIMP```. Caranya tinggal mengcopy folder ```.icons```, ```local```, dan ```var```  ke folder ```/home/$USER```. Semudah itu kawan ✨

# Hasil akhir
## GIMP 2.10 default

![images002](https://res.cloudinary.com/bimagv/image/upload/v1593685294/2020-07/gimp_syaoo3.png "images002")

## GIMP + Patch PhotoGIMP

{% video <iframe width="560" height="315" src="https://res.cloudinary.com/bimagv/video/upload/v1593682553/2020-07/test-photogimp_afxijw.mp4" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> %}

# Kesimpulan
Apakah hasilnya cukup memuaskan? menurut saya **YA**. Untuk kustomisasi pada GIMP yang lain sebetulnya cukup banyak, untuk itu silahkan bereksplorasi dan tanya kepada forum-forum. 

Itu merupakan keuntungan dari aplikasi freeware kebanyakan yaitu kontribusi dan kolaborasi.

Mudah-mudahan bermanfaat untuk kawan-kawan.

(^_^)

# Sumber
- https://github.com/Diolinux/PhotoGIMP